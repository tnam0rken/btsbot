
convert_operation
limit_order_create_operation
limit_order_create2_operation
limit_order_cancel_operation
call_order_update_operation
bid_collateral_operation

fill_order_operation
fill_call_order_operation
fill_settlement_order_operation
execute_bid_operation

class Order {
    constructor(id, amount, receive, rate, expiration) {
        this.id = id;
        this.amount = amount;
        this.receive = receive;
        this.rate = rate;
        this.expiration = expiration;
    }
}

class Fill {
    constructor()
}

class Exchange {



    onOrderCreate(Order) {

    }

    onOrderCancel(op, opBody) {

    }

    onOrderUpdate(op, opBody) {

    }

    onFillOrder(op, opBody) {

    }

    onFillCallOrder(op, opBody) {

    }

    onSettlementOrderOperation(op, opBody) {

    }

    onExecuteBidOperation(op, opBody) {

    }
}