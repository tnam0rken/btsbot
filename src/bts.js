const ga = require("golos-addons");
const CONFIG = ga.global.CONFIG;
const log = ga.global.getLogger("bts");

const Apis = require("bitsharesjs-ws").Apis;
const ChainStore = require("bitsharesjs").ChainStore;
const TransactionHelper = require("bitsharesjs").TransactionHelper;
const TransactionBuilder = require("bitsharesjs").TransactionBuilder;
const Aes = require("bitsharesjs").Aes;
const PrivateKey = require("bitsharesjs").PrivateKey;
//const FetchChain = require("bitsharesjs").FetchChain;
//const ChainStore = require("bitsharesjs").ChainStore;

const WS = CONFIG.ws;


module.exports.init = async function() {
    log.info("set websocket url to", WS);
    return new Promise(resolve => { Apis.instance(WS, true, 30000).init_promise.then(resolve)});
}

async function getBlock(bn) {
    return Apis.instance().db_api().exec( "get_block", [ bn ] );
}

async function getTransaction(bn, tr) {
    return Apis.instance().db_api().exec( "get_transaction", [ bn, tr ] );
}


async function getOpsInBlock(bn) {
    return Apis.instance().db_api().exec( "get_ops_in_block", [ bn, false ] );
}


async function getObject(id) {
    return Apis.instance().db_api().exec( "get_objects", [[ id ]] );
}

async function getAccountHistory(name, limit) {
    return Apis.instance().db_api().exec( "get_account_history", [name, limit] );
}

async function getBalances(bot)  {
    let balances = await Apis.instance().db_api().exec( "get_account_balances", [bot.account_id, [bot.base.asset_id, bot.quote.asset_id]] );

    for(let amount of balances) {
        if(amount.asset_id == bot.base.asset_id) {
            bot.base.amount = amount.amount / Math.pow(10, bot.base.precision);
        }
        if(amount.asset_id == bot.quote.asset_id) {
            bot.quote.amount = amount.amount / Math.pow(10, bot.base.precision);
        }
    }
}

class Order {
    constructor(base, quote, order) {
        this.id = order.id;
        if(order.sell_price.base.asset_id == base.asset_id) {
            this.base = order.for_sale / Math.pow(10, base.precision);
            this.quote = order.sell_price.quote.amount / Math.pow(10, quote.precision);
        } else {
            this.base = order.sell_price.quote.amount / Math.pow(10, base.precision);
            this.quote = order.for_sale / Math.pow(10, quote.precision);
        }
        this.price = this.quote / this.base;
    }
}

async function getUpdateBotInfos(bot)  {
    let res = await Apis.instance().db_api().exec( "get_full_accounts", [[bot.name], false] );
    let acc = res[0][1];
    log.trace(JSON.stringify(acc, null, 4));
    //balances
    for(let amount of acc.balances) {
        if(amount.asset_type == bot.base.asset_id) {
            bot.base.amount = amount.balance / Math.pow(10, bot.base.precision);
        }
        if(amount.asset_type == bot.quote.asset_id) {
            bot.quote.amount = amount.balance / Math.pow(10, bot.quote.precision);
        }
    }
    //orders
    bot.bids = [];
    bot.asks = [];

    for(let order of acc.limit_orders) {
        log.debug(JSON.stringify(order, null, 4));
        if (order.sell_price.base.asset_id == bot.base.asset_id && order.sell_price.quote.asset_id == bot.quote.asset_id) {
            bot.asks.push(new Order(bot.base, bot.quote, order));
        } else if (order.sell_price.base.asset_id == bot.quote.asset_id && order.sell_price.quote.asset_id == bot.base.asset_id) {
            bot.bids.push(new Order(bot.base, bot.quote, order));
        }
        //log.debug(JSON.stringify(bot.asks, null, 4));
        //log.debug(JSON.stringify(bot.bids, null, 4));
    }

    bot.bids.sort((a,b) => {return b.price - a.price;})
    bot.asks.sort((a,b) => {return a.price - b.price;})
    
    //market orders
    bot.market = await Apis.instance().db_api().exec( "get_order_book", [bot.quote.asset_id, bot.base.asset_id, 10]);


    log.trace(JSON.stringify(bot, null, 4));
}

async function cancelOrder(bot, order_id) {
    let nonce = TransactionHelper.unique_nonce_uint64();
    let tx = new TransactionBuilder();
    let op = {
        fee: {
            amount: 0,
            asset_id: '1.3.0'
        },
        fee_paying_account: bot.account_id,
        order: order_id,
        extensions : []
    };
    tx.add_type_operation("limit_order_cancel", op);
    return await processTransaction(bot.key, tx);    
}

async function createOrder(account_id, key, base, quote, amount_to_sell, min_to_receive, price, bot) {

    let nonce = TransactionHelper.unique_nonce_uint64();
    let tx = new TransactionBuilder();
    let expiration = 60 * 60; //one hour
    if (bot && bot.expiration) {
        if (isNaN(bot.expiration)) {
            log.warn("exiration should be entered as number of seconds", bot.expiration);
        } else {
            expiration = bot.expiration;
        }
    }
    log.trace("expiration", expiration);
    let op = {
        fee: {
            amount: 0,
            asset_id: '1.3.0'
        },
        seller : account_id,
        amount_to_sell : {
            amount : (amount_to_sell * Math.pow(10, base.precision)).toFixed(0),
            asset_id : base.asset_id
        },
        min_to_receive : {
            amount : (min_to_receive * Math.pow(10, quote.precision)).toFixed(0),
            asset_id : quote.asset_id
        },
        expiration : new Date(Date.now() + expiration * 1000),
        fill_or_kill : false,
        extensions : []
    };
    log.trace(JSON.stringify(op, null, 4));
    tx.add_type_operation("limit_order_create", op);
    return await processTransaction(key, tx);     

    return true;
}

async function getPriceHistory(base, quote, start, stop, limit) {
    let props = await getProps();
    console.log("curtime =", props.time);
    start = new Date(Date.now()-1000*60*60*24).toISOString().substring(0, 19);
    stop = new Date(Date.now()).toISOString().substring(0, 19);
    console.log("start   =", start);
    console.log("stop    =", props.time);

    //let res = await Apis.instance().db_api().exec( "blockchain_market_price_history", ["BTS", "OPEN.BTC", stop, start, 100] );
    //let res = await Apis.instance().history_api().exec( "get_account_history", [await , "1.11.72180143", 10, "1.11.0"] );
    for(let tr of res) {
        console.log(tr.id, tr.op[0]);
    }
    //let res = await Apis.instance().db_api().exec( "get_full_accounts", [["g-xobot"], false] );
    //console.log("res", res);
    return [];
}

async function getMarketHistory(base, quote, limit) {
    let props = await getProps();
    let start = new Date(Date.now() - 1000 * 60 * 5 * 201).toISOString().substring(0, 19);
    let stop = new Date().toISOString().substring(0, 19);
    log.debug("get_history", base.asset_id, quote.asset_id, start, stop)

    let res = await Apis.instance().history_api().exec("get_market_history", [base.asset_id, quote.asset_id, 300, start, stop]);
    if (!limit) {
        limit = 50;
    }
    log.debug("length", res.length);
    if (res.length > 0) {
        log.debug("key", res[0].key);
    }
    let ret = [];
    for (let i = 0; i < limit; i++) {
        let he = res.pop();
//        log.debug(he);
        if (he) {
            if (he.key.base == base.asset_id) {
                const high_base = he.high_base / Math.pow(10, base.precision);
                const high_quote = he.high_quote / Math.pow(10, quote.precision);
                const low_base = he.low_base / Math.pow(10, base.precision);
                const low_quote = he.low_quote / Math.pow(10, quote.precision);
                const high = parseFloat((high_quote / high_base).toFixed(8));
                const low = parseFloat((low_quote / low_base).toFixed(8));
                
                ret.unshift(
                    {
                        high_base,
                        high_quote,
                        low_base,
                        low_quote,
                        high,
                        low,
                        open: he.key.open
                    }
                )
            } else {
                const high_base = he.high_base / Math.pow(10, quote.precision);
                const high_quote = he.high_quote / Math.pow(10, base.precision);
                const low_base = he.low_base / Math.pow(10, quote.precision);
                const low_quote = he.low_quote / Math.pow(10, base.precision);
                const high = parseFloat((high_base / high_quote).toFixed(8));
                const low = parseFloat((low_base / low_quote).toFixed(8));

                ret.unshift(
                    {
                        high_base,
                        high_quote,
                        low_base,
                        low_quote,
                        high,
                        low,
                        open: he.key.open
                    }
                )
            }
        }
    }
    return ret;
}

async function test(base, quote) {

    await queryAssetID("BTS");
    await queryAssetID("OPEN.ETH");
    process.exit(0);
    let props = await getProps();
    console.log("curtime =", props.time);
    start = new Date(Date.now()-1000*60*60*24*7).toISOString().substring(0, 19);
    stop = new Date(Date.now()).toISOString().substring(0, 19);
    console.log("start   =", start);
    console.log("stop    =", props.time);

    //let res = await Apis.instance().db_api().exec( "blockchain_market_order_history", ["BTS", "OPEN.BTC", stop, start, 100] );
    let res = await Apis.instance().history_api().exec( "get_fill_order_history", ["1.3.0" , "1.3.120", 200]);

    //let res = await Apis.instance().db_api().exec( "get_full_accounts", [["g-xobot"], false] );
    //console.log("res", JSON.stringify(res, null, 4));
    return [];
}

async function queryAssetID(asset) {
    const assetList = await Apis.instance().db_api().exec( "lookup_asset_symbols", [[asset.asset.toUpperCase()]]);
    if (assetList.length == 0 || !assetList[0]) {
        log.error("Asset " + asset.asset + " does not exists!");
        process.exit(1);
    }
    //log.debug(JSON.stringify(assetList, null, 4));
    asset.precision = assetList[0].precision;
    asset.asset_id = assetList[0].id;
    return assetList[0].id;
}

async function queryAccountID(account_name) {
    const acc = await Apis.instance().db_api().exec( "get_account_by_name", [account_name]);
    if(!acc) {
        log.error("Account " + account_name + " does not exists!");
        process.exit(1);
    }
    //log.trace(JSON.stringify(acc, null, 4));
    return acc.id;
}

async function getAssetID(asset) {
    if(!asset.asset_id)  {
        asset.asset_id = await queryAssetID(asset);
    }
    return asset.asset_id;
}

async function queryIDs(bot) {
    const base_asset_id = await getAssetID(bot.base);
    const quote_asset_id = await getAssetID(bot.quote);
    const account_id = await getAccountID(bot);

}

async function getAccountID(bot) {
    if(!bot.account_id) {
        bot.account_id = await queryAccountID(bot.name);
    }
    return bot.account_id;
}

class FilledOrder {
    constructor(base, op) {
        this.base = base.asset_id;
        this.op = (op.pays.asset_id == this.base)?"sell":"buy";
        this.base_amount = 0;
        this.quote_amount = 0;
        switch(this.op) {
            case "sell":
                this.base_amount = op.pays.amount;
                this.quote_amount = op.receives.amount;
                break;
            case "buy":
                this.base_amount = op.receives.amount;
                this.quote_amount = op.pays.amount;
                break;
        }
    }
}

async function getFilledOrders(bot) {
    let hist = await Apis.instance().history_api().exec( "get_fill_order_history", [bot.base.asset_id , bot.quote.asset_id, 20]);
    log.trace("fill_order_history", JSON.stringify(hist, null, 4));
    let ret = [];

    if (!bot.sequence) {
        bot.sequence = hist[0].key.sequence;
        log.debug("bot has not yet read history, last sequence", bot.sequence);
        return ret;
    }
    
    let max_sequence = bot.sequence;
    for (let he of hist) {
        if(he.key.sequence >= max_sequence) {
            break;
        }
        if(he.op.account_id == bot.account_id) {
            ret.push(new FilledOrder(bot.base, he.op));
        }
        if (bot.sequence > he.key.sequence) {
            bot.sequence = he.key.sequence;
        }
    }

    return ret;
}

/*
method: "call", params: [4, "get_account_history", ["1.2.36449", "1.11.72173461", 100, "1.11.0"]],…}
id
:
272
method
:
"call"
params
:
[4, "get_account_history", ["1.2.36449", "1.11.72173461", 100, "1.11.0"]]
*/

async function getAccount(name) {
    return Apis.instance().db_api().exec( "get_full_accounts", [[ name ], false] );
}

async function getProps() {
    const props = Apis.instance().db_api().exec( "get_dynamic_global_properties", [] );
    

    return props;
}

async function processTransaction(key, tx) {
    const privKey = PrivateKey.fromWif(key);
    await tx.set_required_fees();
    tx.add_signer(privKey, privKey.toPublicKey().toPublicKeyString());
    let x = await tx.broadcast();
    return x;
}

module.exports.getBlock = getBlock;
module.exports.getTransaction = getTransaction;
module.exports.getOpsInBlock = getOpsInBlock;
module.exports.getObject = getObject;
module.exports.getAccount = getAccount;
module.exports.getAccountHistory = getAccountHistory;
module.exports.getProps = getProps;
module.exports.getMarketHistory = getMarketHistory;
module.exports.test = test;
module.exports.getFilledOrders = getFilledOrders;
module.exports.queryIDs = queryIDs;
module.exports.getBalances = getBalances;
module.exports.getUpdateBotInfos = getUpdateBotInfos;
module.exports.cancelOrder = cancelOrder;
module.exports.createOrder = createOrder;