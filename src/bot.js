const fs = require('fs');
const ga = require("golos-addons");
const global = ga.global;

const bts = require("./bts");
const db = require("./db");

const log = ga.global.getLogger("btsnotifier");

const CONFIG = global.CONFIG;
const BOTS = CONFIG.bots;

const MINDIFF = CONFIG.mindiff || 0.002;

function write_stat(filename, ...values) {
    
    let row = "";
    let comma = "";
    for (let v of values) {
        row += comma + '"' + v + '"';
        comma = ";"
    }
    log.debug("write stats", filename, row);
    fs.appendFile(filename, row + "\n", function (err) {
        if (err) throw err;
        log.debug("saved stat", filename);
    });    
}


class Bot {
    constructor(config) {
        Object.assign(this, config);
        this.base.amount = 0;
        this.quote.amount = 0;
        this.lastOrder = 0;
    }

    get logName() {
        return this.name + ": ";
    }

    warn() {
        log.warn(this.logName, ...arguments);
    }

    info() {
        log.info(this.logName, ...arguments);
    }

    error() {
        log.error(this.logName, ...arguments);
    }

    debug() {
        log.debug(this.logName, ...arguments);
    }

    trace() {
        log.trace(this.logName, ...arguments);
    }

    async init() {
        await bts.queryIDs(this);
        await bts.getUpdateBotInfos(this);
    }

    async cancelBotOrders() {
        //cancel if configured
        if (this.cancelOrdersOnStart) {
            while (this.bids.length > 0) {
                let order = this.bids.pop();
                this.info("canceling bid ", "at price", order.price.toFixed(6));
                await bts.cancelOrder(this, order.id);
            }
            while (this.asks.length > 0) {
                let order = this.asks.pop();
                this.info("canceling ask ", "at price", order.price.toFixed(6));
                await bts.cancelOrder(this, order.id);
            }
        }
    }

    async checkFilledOrders() {
        let orders = await bts.getFilledOrders(this);

        for (let order of orders) {
            this.lastOrder = Date.now();
            const ba = order.base_amount / Math.pow(10, this.base.precision);
            const qa = order.quote_amount / Math.pow(10, this.quote.precision);
            this.info("filled order", order.op, ba.toFixed(this.base.precision), qa.toFixed(this.quote.precision), (qa / ba).toFixed(6));
            if (this.stats) {
                await bts.getUpdateBotInfos(this);
                const base_reserve = await this.processGreedy(this.base);
                const quote_reserve = await this.processGreedy(this.quote);
                log.debug("bids", this.bids);
                log.debug("asks", this.asks);
                write_stat(this.name + "_filled_orders.csv",
                    (new Date()).toISOString(),
                    order.op,
                    ba.toFixed(this.base.precision),
                    qa.toFixed(this.quote.precision),
                    (qa / ba).toFixed(6),
                    this.base.amount,
                    this.base.asset,
                    base_reserve,
                    (this.asks[0]) ? this.asks[0].base : 0,
                    this.quote.amount,
                    this.quote.asset,
                    quote_reserve,
                    (this.bids[0]) ? this.bids[0].quote : 0,
                );
            }
        }
    }

    getBrain(brainName) {
        if (!this.brain) {
            return null;
        }
        //this.debug(this.brain);
        let brain = this.brain[brainName];
        if (brain && brain.enabled) {
            return brain;
        }
        return null;
    }

    /**
     * returns true if order is own or very similar
     * @param {*} order 
     * @param {*} ownorders 
     */
    compareOrders(order, ownorders) {
        //order.price
        //order.quote
        //order.base
        for (let own of ownorders) {
            log.trace("order", JSON.stringify(order));
            log.trace("own", JSON.stringify(own));
            if (own.base == order.base && own.quote == order.quote || own.base == order.quote || own.quote == order.base) {
                return true;
            }
        }
        return false;
    }

    async getTopPrices() {

        let brain = this.getBrain("basePrice");

        if (!brain) {
            this.trace("use default basePrice.top strategy");
            brain = {
                strategy: "top"
            }
        }

        switch (brain.strategy) {
            case "top":
                this.trace("using top basePrice strategy");
                if(!brain.min_bid_amount) {
                    brain.min_bid_amount = 0;
                }
                if(!brain.min_ask_amount) {
                    brain.min_ask_amount = 0;
                }
                let market_top_bid = null;
                let market_top_ask = null;

                this.debug("min_bid_amount", brain.min_bid_amount);
                this.debug("min_ask_amount", brain.min_ask_amount);

                for(let i = 0; i < this.market.bids.length; i++) {
                    if(parseFloat(this.market.bids[i].base) > brain.min_bid_amount && !this.compareOrders(this.market.bids[i], this.bids)) {
                        log.debug("amount is ok", parseFloat(this.market.bids[i].base), this.market.bids[i].price);
                        market_top_bid = parseFloat(this.market.bids[i].price);
                        break;
                    } else {
                        log.debug("amount is less then desired or own order", parseFloat(this.market.bids[i].base), this.market.bids[i].price);
                    }
                }

                for(let i = 0; i < this.market.asks.length; i++) {
                    if(parseFloat(this.market.asks[i].quote) > brain.min_ask_amount && !this.compareOrders(this.market.asks[i], this.asks)) {
                        log.debug("amount is ok", parseFloat(this.market.asks[i].quote), this.market.asks[i].price);
                        market_top_ask = parseFloat(this.market.asks[i].price);
                        break;
                    } else {
                        log.debug("amount is less then desired or own order", parseFloat(this.market.asks[i].quote), this.market.asks[i].price);
                    }
                }

                return {
                    market_top_bid,
                    market_top_ask
                }
            case "sma":
                this.trace("using sma basePrice strategy");
                if (!brain.count) {
                    this.warn("brain.basePrice.count is not defined, using 5");
                    brain.count = 5;
                }
                if (brain.count < 5) {
                    this.warn("brain.basePrice.count < 5!, using 5");
                    brain.count = 5;
                }
                if (brain.count > 100) {
                    this.warn("brain.basePrice.count > 100!, using 100");
                    brain.count = 100;
                }
                const history = await bts.getMarketHistory(
                    this.base,
                    this.quote, brain.count);
                
                this.debug("sma len", history.length)
                if (history.length == 0) {
                    //return something
                    return {
                        market_top_bid: parseFloat(this.market.bids[0].price),
                        market_top_ask: parseFloat(this.market.asks[0].price)
                    }
                }
                let sum = 0;
                for (let i = 0; i < history.length; i++) {
                    let he = history[i];
                    //this.debug("he", he);
                    const avg_price = (he.low + he.high) / 2;
                    sum += avg_price;
                }
                const sma = sum / history.length;
                this.debug("sma = ", sma);
                return {
                    market_top_bid: sma,
                    market_top_ask: sma
                };
            case "avg":
                this.trace("using avg basePrice strategy");
                if (!brain.count) {
                    this.warn("brain.basePrice.count is not defined, using 1");
                    brain.count = 1;
                }
                if (brain.count < 1) {
                    this.warn("brain.basePrice.count < 1!, using 1");
                    brain.count = 1;
                }
                if (brain.count > 10) {
                    this.warn("brain.basePrice.count > 10!, using 10");
                    brain.count = 10;
                }
                let sum_bid = 0;
                let sum_ask = 0;
                let count_bid = Math.min(brain.count, this.market.bids.length);
                let count_ask = Math.min(brain.count, this.market.asks.length);
                let count_bid_sum = 0;
                let count_ask_sum = 0;
                for (let i = 0; i < count_bid; i++) {
                    if (!this.compareOrders(this.market.bids[i], this.bids)) {
                        sum_bid += parseFloat(this.market.bids[i].price);
                        count_bid_sum++;
                    } else {
                        count_bid++;
                        log.trace("ignore own order");
                    }
                }
                for (let i = 0; i < count_ask; i++) {
                    if (!this.compareOrders(this.market.asks[i], this.asks)) {
                        sum_ask += parseFloat(this.market.asks[i].price);
                        count_ask_sum++;
                    } else {
                        count_ask++;
                        log.trace("ignore own order");
                    }
                }
                this.debug(brain.count,
                    "\n", sum_bid, count_bid_sum, sum_bid / count_bid_sum,
                    "\n", sum_ask, count_ask_sum, sum_ask / count_ask_sum);
                return {
                    market_top_bid: sum_bid / count_bid_sum,
                    market_top_ask: sum_ask / count_ask_sum
                }
            case "offset":
                this.trace("using offset basePrice strategy");
                if (!brain.offset) {
                    this.warn("brain.basePrice.offset is not defined, using 0");
                    brain.offset = 0;
                }
                if (brain.offset < 0) {
                    this.warn("brain.basePrice.offset < 0!, using 0");
                    brain.offset = 0;
                }
                if (brain.offset > 9) {
                    this.warn("brain.basePrice.offset > 9!, using 9");
                    brain.offset = 9;
                }
                let offset_bid = Math.min(brain.offset, this.market.bids.length);
                let offset_ask = Math.min(brain.offset, this.market.asks.length);
                return {
                    market_top_bid: parseFloat(this.market.bids[offset_bid].price),
                    market_top_ask: parseFloat(this.market.asks[offset_ask].price)
                }
        }
    }


    checkLimits(bidask, price, basePrice) {
        let brain = this.getBrain("limits");
        if (!brain) {
            return price;
        }

        const limits = brain[bidask];

        const limitless = !!(brain.limitless);
        if (!limitless) {
            if (limits.min > basePrice || limits.max < basePrice) {
                if (!this.limitWarned) {
                    this.warn("base price exeeds limits", bidask, basePrice);
                }
                this.limitWarned = true; //warn only once
                return -1;
            }
            this.limitWarned = false;
        }
        
        if (limits.min > price) {
            return limits.min;
        } else if (limits.max < price) {
            return limits.max;
        }
        return price;
    }

    async createOrderChecked(askbid, base, quote, top_price, desired_price, max) {
        this.debug("create order base", base.asset);
        const reserve = await this.processGreedy(base)
        let order_max = base.max;
        if (max) {
            order_max = Math.min(order_max, max);
        }

        this.debug("reserved", reserve, "min", base.min, "max", order_max, "amount", base.amount);

        if (base.amount < reserve + base.min) {
            this.debug(" has not enough amount ", base.asset);
            return false;
        }
        this.info(
            askbid,
            "top", top_price.toFixed(8),
            "desired", desired_price.toFixed(8)
        );

        const amount_to_sell = Math.min(base.amount - reserve, order_max);
        await this.createOrder(base, quote, desired_price, amount_to_sell);
    }

    async evaluateTrend(elastic) {
        const history = await bts.getMarketHistory(
            this.base,
            this.quote, 50);
        log.debug("len", history.length)
        if (history.length == 0) {
            return 0;
        }
        let sum50 = 0;
        let sum5 = 0;
        let count5 = 0;
        for (let i = 0; i < history.length; i++) {
            let he = history[i];
            const avg_price = (he.low + he.high) / 2;
            sum50 += avg_price;
            if ((history.length - i) <= 5) {
                sum5 += avg_price;
                count5++;
            }
        }
        let ma50 = sum50 / history.length;
        let ma5 = sum5 / count5;
        let diff = (ma50 - ma5) * 100 / ma50;
        log.debug(history[history.length - 1].open, ma50.toFixed(8), history.length, ma5.toFixed(8), count5, diff.toFixed(2))
        return diff;
    }

    async getSpreadValues() {

        //assume, the max trend ~15%
        const MAX_TREND = 15;

        let brain = this.getBrain("elastic");
        if (!brain) {
            return {
                base_percent: this.base.percent,
                quote_percent: this.quote.percent,
                base_max: this.base.max,
                quote_max: this.quote.max
            };
        }

        const trend = await this.evaluateTrend(brain);
        this.debug("elastic trend", trend);
        let base_coefficient = 0;
        let quote_coefficient = 0;
        if (trend > 0) {
            base_coefficient = trend;
        } else {
            quote_coefficient = trend * (-1);
        }

        this.debug("base_coeficient", base_coefficient, "quote_coefficient", quote_coefficient);

        const base_percent_range = this.base.percent - brain.base.min_percent;
        const quote_percent_range = this.quote.percent - brain.quote.min_percent;

        this.debug("base_percent_range", base_percent_range, "quote_percent_range", quote_percent_range);

        const base_count_range = brain.base.max_orders - brain.base.min_orders;
        const quote_count_range = brain.quote.max_orders - brain.quote.min_orders;

        this.debug("base_count_range", base_count_range, "quote_count_range", quote_count_range);

        const base_liquid_amount = this.base.amount - await this.processGreedy(this.base);
        const quote_liquid_amount = this.quote.amount - await this.processGreedy(this.quote);

        this.debug("base_liquid_amount", base_liquid_amount, "quote_liquid_amount", quote_liquid_amount);

        const base_percent = (base_coefficient * base_percent_range / MAX_TREND) + brain.base.min_percent;
        const quote_percent = (quote_coefficient * quote_percent_range / MAX_TREND) + brain.quote.min_percent;

        this.debug("base_percent", base_percent, "quote_percent", quote_percent);

        const base_count = (base_coefficient * base_count_range / MAX_TREND) + brain.base.min_orders;
        const quote_count = (quote_coefficient * quote_count_range / MAX_TREND) + brain.quote.min_orders;

        this.debug("base_count", base_count, "quote_count", quote_count);

        const base_max = base_liquid_amount / base_count;
        const quote_max = quote_liquid_amount / quote_count;

        this.debug("base_max", base_max, "quote_max", quote_max);

        return {
            base_percent,
            quote_percent,
            base_max,
            quote_max
        };
    }

    getStretchValue() {
        let brain = this.getBrain("stretch");
        if (!brain) {
            this.debug("no stretch brain");
            return 1;
        }

        if (!brain.percent) {
            brain.percent = 0;
        }
        if (!brain.delay) {
            brain.delay = 1;
        }

        const diff = brain.delay - Math.min(brain.delay,(Date.now() - this.lastOrder)/1000);
        const stretch = (brain.percent * diff / brain.delay) / 100 + 1;
        this.debug("calculated stretch value", brain.percent, brain.delay, diff, stretch);
        if (isNaN(stretch)) {
            this.warn("unable to calculate stretch value", brain.percent, brain.delay, diff, stretch);
            return 1;
        }
        return stretch;
    }

    async process() {
        await bts.getUpdateBotInfos(this);
        this.trace("processing " + this.name);

        this.trace("market", this.market);
        this.trace("bids", this.bids);
        this.trace("brain", this.brain);

        const { market_top_bid, market_top_ask } = await this.getTopPrices();

        this.debug("base prices", market_top_bid, market_top_ask);

        if(isNaN(market_top_bid) || isNaN(market_top_ask)) {
            this.warn("could not determine base prices (bid, ask)", market_top_bid, market_top_ask);
            return;
        }

        const { base_percent, quote_percent, base_max, quote_max } = await this.getSpreadValues();

        this.debug("spread values", base_percent, quote_percent, base_max, quote_max);

        const stretch = this.getStretchValue();
        
        this.debug("stretch value", stretch);

        let desired_bid_price = this.checkLimits("bid", market_top_bid / (1 + (base_percent * stretch) / 100), market_top_bid);
        let desired_ask_price = this.checkLimits("ask", market_top_ask * (1 + (quote_percent * stretch) / 100), market_top_ask);

        this.debug("desired_prices", desired_bid_price, desired_ask_price);

        let mybid = this.bids.length > 0 ? this.bids[0] : null;
        let myask = this.asks.length > 0 ? this.asks[0] : null;

        const mindiff_bid = MINDIFF * (this.base.percent >= 0)?1:-1;
        const cancel_bid = (mybid && mybid.price < desired_bid_price - desired_bid_price * mindiff_bid)

        if (desired_bid_price < 0 || cancel_bid) {
            if (mybid) {
                this.info("cancel bid",
                    this.base.asset,
                    this.quote.asset,
                    mybid.price.toFixed(6),
                    desired_bid_price,
                    (mybid.price-desired_bid_price).toFixed(6),
                    cancel_bid);
                await bts.cancelOrder(this, mybid.id);
            }
        } else {
            mybid && this.debug("dont cancel bid", mindiff_bid, cancel_bid, "actually", mybid.price, "shouldbe", desired_bid_price)
        }

        if(!isNaN(market_top_bid)) {
            if (desired_bid_price > 0) {
                if (this.bids.length == 0) {
                    await this.createOrderChecked("bid", this.quote, this.base, market_top_bid, desired_bid_price, quote_max);
                }
            }
        } else {
            log.warn("was unable to determine base bid price!");
        }
        const mindiff_ask = MINDIFF * (this.quote.percent >= 0)?1:-1;
        const cancel_ask = (myask && myask.price > desired_ask_price + desired_ask_price * mindiff_ask);
        if (desired_ask_price < 0 || cancel_ask) {
            if (myask) {
                this.info("cancel ask",
                    this.base.asset,
                    this.quote.asset,
                    myask.price.toFixed(6),
                    desired_bid_price,
                    (myask.price - desired_bid_price).toFixed(6),
                    cancel_ask);
                await bts.cancelOrder(this, this.asks[0].id);
            }
        } else {
            myask && this.debug("dont cancel ask", mindiff_ask, cancel_ask, "actually", myask.price, "shouldbe", desired_ask_price)
        }

        if(!isNaN(market_top_ask)) {
            if (desired_ask_price > 0) {
                if (this.asks.length == 0) {
                    await this.createOrderChecked("ask", this.base, this.quote, market_top_ask, desired_ask_price, base_max);
                }
            }
        } else {
            log.warn("was unable to determine base ask price!");
        }
    }

    async processGreedy(base) {

        const brain = this.getBrain("greedy");
        if (!brain) {
            this.trace("greedy is disabled");
            return base.reserve;
        }

        let asset = "base";
        if (this.base.asset_id != base.asset_id) {
            asset = "quote";
        }

        this.trace("greedy's asset", asset);

        let memory = await db.recallGreedy(this.name);
        if (!memory) {
            this.trace("no memory in mind, use defaults");
            memory = {
                base: this.base.reserve,
                quote : this.quote.reserve
            }
        }



        let reserve = base.amount * brain[asset] / 100;
        this.trace("calculated reserve", reserve, "in memory", memory[asset]);
        if (reserve > memory[asset]) {
            this.debug("new reserve is greater then memorized");
            memory[asset] = reserve;
            await db.memorizeGreedy(this.name, memory);
        }
        
        return memory[asset];
    }

    async createOrder(base, quote, price, amount_to_sell) {
        let min_to_receive = amount_to_sell * price;
        this.debug("base.asset_id", base.asset_id, "this.quote.asset_id", this.quote.asset_id);
        if (base.asset_id == this.quote.asset_id) {
            this.debug("reverse price");
            min_to_receive = amount_to_sell / price;
        }

        this.info("create order", "sell", amount_to_sell.toFixed(base.precision), base.asset, "receive", min_to_receive.toFixed(quote.precision), quote.asset, "price", price.toFixed(8));
        this.debug("this.account_id", this.account_id);
        await bts.createOrder(this.account_id, this.key, base, quote, amount_to_sell, min_to_receive, price, this);
    }
}

module.exports = Bot;