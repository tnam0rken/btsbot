const nedb = require("nedb");

const ga = require("golos-addons");
const global = ga.global;

const log = ga.global.getLogger("db");

const CONFIG = global.CONFIG;
const BOTS = CONFIG.bots;

const dbGreedy = new nedb({ filename: 'greedy.db', autoload: true });

async function recallGreedy(account) {
    return new Promise(resolve => {
        dbGreedy.findOne({ account }, function (err, thoughts) {
            if (err) {
                throw err;
            }
            
            if (thoughts) {
                resolve(thoughts);
            }
            resolve(null);
        });
    });
}

async function memorizeGreedy(account, thoughts) {
    thoughts.account = account;
    dbGreedy.update({ account }, thoughts, { upsert: true }, function (err, numReplaced) {
        log.debug("updated greedy");
    });
}

module.exports.recallGreedy = recallGreedy;
module.exports.memorizeGreedy = memorizeGreedy;
