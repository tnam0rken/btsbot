const ga = require("golos-addons");
const global = ga.global;
ga.global.initApp("btsnotifier");

const bts = require("./src/bts");
const Bot = require("./src/bot");

const log = ga.global.getLogger("btsnotifier");

const CONFIG = global.CONFIG;

async function test() {
    await bts.init();
    const history = await bts.getMarketHistory(
        { asset_id: "1.3.121", precision: 4 },
        { asset_id: "1.3.113", precision: 4 }, 50);
    log.info("len", history.length)
    if (history.length == 0) {
        return 0;
    }
    let sum50 = 0;
    let sum5 = 0;
    let count5 = 0;
    for (let i = 0; i < history.length; i++) {
        let he = history[i];
        log.debug(he)
        const avg_price = (he.low + he.high) / 2;
        sum50 += avg_price;
        if ((history.length - i) <= 5) {
            sum5 += avg_price;
            count5++;
        }
    }
    let ma50 = sum50 / history.length;
    let ma5 = sum5 / count5;
    let diff = (ma50 - ma5)*100/ma50;
    log.debug(history[history.length - 1].open, ma50.toFixed(8), history.length, ma5.toFixed(8), count5, diff.toFixed(2))
    return diff;
}


test();




