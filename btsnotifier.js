const ga = require("golos-addons");
const global = ga.global;
ga.global.initApp("btsnotifier");

const bts = require("./src/bts");
const Bot = require("./src/bot");

const log = ga.global.getLogger("btsnotifier");

const CONFIG = global.CONFIG;

log.debug("CONFIG", JSON.stringify(CONFIG, null, 4));

const CONFIGBOTS = CONFIG.bots;
const BOTS = [];

let WORKING = true;

async function checkFilledOrders() {
    for(let bot of BOTS) {
        await bot.checkFilledOrders();
    }
}

async function cancelAllOrders() {
    for(let bot of BOTS) {
        await bot.cancelBotOrders();
    }
}

async function updateOrders() {
    log.trace("updateOrders");
    await checkFilledOrders();

    if(WORKING) {
        for(let bot of BOTS) {
            try {
                await bot.process();
            } catch(e) {
                log.error("error in ", bot.name, e);
            }
        }
    }

}

function checkAssets(ass1, ass2) {
    for(let bot of BOTS) {
        if(bot.base.asset_id == ass1 && bot.quote.asset_id == ass2) {
            return true;
        }
        if(bot.base.asset_id == ass2 && bot.quote.asset_id == ass1) {
            return true;
        }
    }
    return false;
}

async function processBlock(bn) {
    log.debug("processing block " + bn);
    let block = await bts.getBlock(bn);
    let found_order_changes = false;
    for(let tr of block.transactions) {
        //log.debug("tr", JSON.stringify(tr, null, 4));
        for(let rawOp of tr.operations) {
            let op = rawOp[0];
            let opBody = rawOp[1];
            log.trace("op", op);
            switch(op) {
                case 1: //limit_order_create_operation
                    found_order_changes = (!checkAssets(opBody.amount_to_sell.asset_id, opBody.min_to_receive.asset_id));
                    break;
            }
        }
    }             
    if(found_order_changes)  {
        await updateOrders();
    }
}

async function updateBots() {
    for (let cb of CONFIGBOTS) {
        const bot = new Bot(cb);
        await bot.init();
        BOTS.push(bot);
    }    
}

let currentBlock = 0;

async function run() {

    await bts.init();
    await updateBots();
    await cancelAllOrders();

    let props = await bts.getProps();
    currentBlock = props.head_block_number;

    while(true) {
        try {

            props = await bts.getProps();

            if(props.head_block_number < currentBlock) {
                //log.info(`no new blocks, skip round`);
                await ga.global.sleep(1000*6);
                continue;
            }

            await processBlock(currentBlock++);

        } catch(e) {
            log.error("Error catched in main loop!");
            log.error(e);
        }  
    } 
    process.exit(1);
}

let lastTime = Date.now();
let lastBlock = currentBlock;

const watchdog = () => {
    log.debug("watchdog", lastBlock, currentBlock);
    if (currentBlock == lastBlock) {
        log.error("stopped to receive new blocks");
        process.exit(1);
    }
    lastBlock = currentBlock;
}

run();

//watchdog
setInterval(watchdog, 9000);



